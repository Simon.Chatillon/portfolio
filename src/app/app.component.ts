import { Component, ViewChild } from '@angular/core';
import { SquidService } from './utils/squid';
import { SkillsComponent } from './skills/skills.component';
import { LanguagesComponent } from './languages/languages.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  cubeClass = 'home';

  @ViewChild(SkillsComponent) skillsComponent: SkillsComponent;

  @ViewChild(LanguagesComponent) languagesComponent: LanguagesComponent;

  facesDisplay = {
    'home': 'frontFace',
    'experience': '',
    'skills': '',
    'training': '',
    'languages': '', 
    'about': '',
  };

  constructor(private squid: SquidService) {}

  ngOnInit() {
    this.squid.init(false);
  }

  switchFace(face: string) {
    var me=this;

    this.cubeClass = face;

    if(face === "skills") {
      this.skillsComponent.initGauges();
    } else if(face === "languages") {
      this.languagesComponent.initGauges();
    }
    this.onPageChange();
  }

  previousFace() {
    switch(this.cubeClass) {
      case "skills":
        this.cubeClass = "experience";
        document.getElementById("skillsButton").classList.remove("currentNavOption");
        document.getElementById("experienceButton").classList.add("currentNavOption");
        break;
      case "training":
        this.cubeClass = "skills";
        document.getElementById("trainingButton").classList.remove("currentNavOption");
        document.getElementById("skillsButton").classList.add("currentNavOption");
        this.skillsComponent.initGauges();
        break;
      case "languages":
        this.cubeClass = "training";
        document.getElementById("languagesButton").classList.remove("currentNavOption");
        document.getElementById("trainingButton").classList.add("currentNavOption");
        break;
      case "about":
        this.cubeClass = "languages";
        document.getElementById("aboutButton").classList.remove("currentNavOption");
        document.getElementById("languagesButton").classList.add("currentNavOption");
        this.languagesComponent.initGauges();
        break;
      case "home":
        this.cubeClass = "about";
        document.getElementById("homeButton").classList.remove("currentNavOption");
        document.getElementById("aboutButton").classList.add("currentNavOption");
        break;
      case "experience":
        this.cubeClass = "home";
        document.getElementById("experienceButton").classList.remove("currentNavOption");
        document.getElementById("homeButton").classList.add("currentNavOption");
        break;
    }
    this.onPageChange();
  }

  nextFace() {
    switch(this.cubeClass) {
      case "home":
        this.cubeClass = "experience";
        document.getElementById("homeButton").classList.remove("currentNavOption");
        document.getElementById("experienceButton").classList.add("currentNavOption");
        break;
      case "experience":
        this.cubeClass = "skills";
        document.getElementById("experienceButton").classList.remove("currentNavOption");
        document.getElementById("skillsButton").classList.add("currentNavOption");
        this.skillsComponent.initGauges();
        break;
      case "skills":
        this.cubeClass = "training";
        document.getElementById("skillsButton").classList.remove("currentNavOption");
        document.getElementById("trainingButton").classList.add("currentNavOption");
        break;
      case "training":
        this.cubeClass = "languages";
        document.getElementById("trainingButton").classList.remove("currentNavOption");
        document.getElementById("languagesButton").classList.add("currentNavOption");
        this.languagesComponent.initGauges();
        break;
      case "languages":
        this.cubeClass = "about";
        document.getElementById("languagesButton").classList.remove("currentNavOption");
        document.getElementById("aboutButton").classList.add("currentNavOption");
        break;
      case "about":
        this.cubeClass = "home";
        document.getElementById("aboutButton").classList.remove("currentNavOption");
        document.getElementById("homeButton").classList.add("currentNavOption");
        break;
    }
    this.onPageChange();
  }

  onPageChange() {
    for(var page in this.facesDisplay) {
      this.facesDisplay[page] = "";
    }
    this.facesDisplay[this.cubeClass] = "frontFace";
  }

}
