import { Component, OnInit } from '@angular/core';

declare const Gauge;

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  angularGauge;
  ionicGauge;
  sigGauge;
  htmlCssGauge;
  gitGauge;
  androidGauge;
  springGauge;
  sqlGauge;
  awsGauge;

  constructor() { }

  ngOnInit() {
    this.angularGauge = this.createGauge("angularGauge");
    this.ionicGauge = this.createGauge("ionicGauge");
    this.sigGauge = this.createGauge("sigGauge");
    this.htmlCssGauge = this.createGauge("htmlCssGauge");
    this.gitGauge = this.createGauge("gitGauge");
    this.androidGauge = this.createGauge("androidGauge");
    this.springGauge = this.createGauge("springGauge");
    this.sqlGauge = this.createGauge("sqlGauge");
    this.awsGauge = this.createGauge("awsGauge");
  }

  createGauge(elementId: string) {
    return Gauge(
      document.getElementById(elementId), {
        max: 100,
        dialStartAngle: -90,
        dialEndAngle: -90.001,
        value: 0,
        showValue: false,
        label: function(value) {
          return Math.round(value * 100) / 100;
        }
      }
    );
  }

  initGauges() {
      this.angularGauge.setValueAnimated(90, 1);
      this.ionicGauge.setValueAnimated(80, 1);
      this.sigGauge.setValueAnimated(95, 1);
      this.htmlCssGauge.setValueAnimated(90, 1);
      this.gitGauge.setValueAnimated(90, 1);
      this.androidGauge.setValueAnimated(80, 1);
      this.springGauge.setValueAnimated(70, 1);
      this.sqlGauge.setValueAnimated(75, 1);
      this.awsGauge.setValueAnimated(65, 1);
  }

}
