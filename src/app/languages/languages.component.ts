import { Component, OnInit } from '@angular/core';

declare const Gauge;

@Component({
  selector: 'app-languages',
  templateUrl: './languages.component.html',
  styleUrls: ['./languages.component.scss']
})
export class LanguagesComponent implements OnInit {

  frenchGauge;
  englishGauge;
  italianGauge;

  constructor() { }

  ngOnInit() {
    this.frenchGauge = this.createGauge("frenchGauge");
    this.englishGauge = this.createGauge("englishGauge");
    this.italianGauge = this.createGauge("italianGauge");
  }

  createGauge(elementId: string) {
    return Gauge(
      document.getElementById(elementId), {
        max: 100,
        dialStartAngle: -90,
        dialEndAngle: -90.001,
        value: 0,
        showValue: false,
        label: function(value) {
          return Math.round(value * 100) / 100;
        }
      }
    );
  }

  initGauges() {
    this.frenchGauge.setValueAnimated(100, 1);
    this.englishGauge.setValueAnimated(90, 1);
    this.italianGauge.setValueAnimated(75, 1);
  }

}
